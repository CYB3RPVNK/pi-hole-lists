# Pi Hole lists

Lists for pi-hole

Mostly taken from: https://wally3k.github.io/

## Full blocklist from: https://wally3k.github.io/

* Suspicious Lists

    * https://hosts-file.net/grm.txt
    * https://reddestdream.github.io/Projects/MinimalHosts/etc/MinimalHostsBlocker/minimalhosts
    * https://raw.githubusercontent.com/StevenBlack/hosts/master/data/KADhosts/hosts
    * https://raw.githubusercontent.com/StevenBlack/hosts/master/data/add.Spam/hosts
    * https://v.firebog.net/hosts/static/w3kbl.txt
    * https://v.firebog.net/hosts/BillStearns.txt
    * http://sysctl.org/cameleon/hosts
    * https://raw.githubusercontent.com/CHEF-KOCH/BarbBlock-filter-list/master/HOSTS.txt
    * https://www.dshield.org/feeds/suspiciousdomains_Low.txt
    * https://www.dshield.org/feeds/suspiciousdomains_Medium.txt
    * https://www.dshield.org/feeds/suspiciousdomains_High.txt
    * https://www.joewein.net/dl/bl/dom-bl-base.txt
    * https://raw.githubusercontent.com/matomo-org/referrer-spam-blacklist/master/spammers.txt
    * https://hostsfile.org/Downloads/hosts.txt
    * https://someonewhocares.org/hosts/zero/hosts
    * https://raw.githubusercontent.com/Dawsey21/Lists/master/main-blacklist.txt
    * https://raw.githubusercontent.com/vokins/yhosts/master/hosts
    * http://winhelp2002.mvps.org/hosts.txt
    * https://hostsfile.mine.nu/hosts0.txt
    * https://v.firebog.net/hosts/Kowabit.txt
    * https://adblock.mahakala.is

* Advertising Lists

    * https://adaway.org/hosts.txt
    * https://v.firebog.net/hosts/AdguardDNS.txt
    * https://raw.githubusercontent.com/anudeepND/blacklist/master/adservers.txt
    * https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt
    * https://hosts-file.net/ad_servers.txt
    * https://v.firebog.net/hosts/Easylist.txt
    * https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts;showintro=0
    * https://raw.githubusercontent.com/StevenBlack/hosts/master/data/UncheckyAds/hosts
    * https://www.squidblacklist.org/downloads/dg-ads.acl
    * https://raw.githubusercontent.com/CHEF-KOCH/CKs-FilterList/master/HOSTS/CK's-Spotify-HOSTS-FilterList.txt

* Tracking & Telemetry Lists

    * https://v.firebog.net/hosts/Easyprivacy.txt
    * https://v.firebog.net/hosts/Prigent-Ads.txt
    * https://raw.githubusercontent.com/quidsup/notrack/master/trackers.txt
    * https://raw.githubusercontent.com/StevenBlack/hosts/master/data/add.2o7Net/hosts
    * https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/spy.txt
    * https://v.firebog.net/hosts/Airelle-trc.txt
    * https://raw.githubusercontent.com/Perflyst/PiHoleBlocklist/master/android-tracking.txt
    * https://raw.githubusercontent.com/Perflyst/PiHoleBlocklist/master/SmartTV.txt
    * https://raw.githubusercontent.com/CHEF-KOCH/Canvas-Font-Fingerprinting-pages/master/Canvas.txt
    * https://raw.githubusercontent.com/CHEF-KOCH/WebRTC-tracking/master/WebRTC.txt
    * https://raw.githubusercontent.com/CHEF-KOCH/Audio-fingerprint-pages/master/AudioFp.txt
    * https://raw.githubusercontent.com/CHEF-KOCH/Canvas-fingerprinting-pages/master/Canvas.txt

* Malicious Lists

    * https://s3.amazonaws.com/lists.disconnect.me/simple_malvertising.txt
    * https://mirror1.malwaredomains.com/files/justdomains
    * https://hosts-file.net/exp.txt
    * https://hosts-file.net/emd.txt
    * https://hosts-file.net/psh.txt
    * https://mirror.cedia.org.ec/malwaredomains/immortal_domains.txt
    * https://www.malwaredomainlist.com/hostslist/hosts.txt
    * https://bitbucket.org/ethanr/dns-blacklists/raw/8575c9f96e5b4a1308f2f12394abd86d0927a4a0/bad_lists/Mandiant_APT1_Report_Appendix_D.txt
    * https://v.firebog.net/hosts/Prigent-Malware.txt
    * https://v.firebog.net/hosts/Prigent-Phishing.txt
    * https://raw.githubusercontent.com/quidsup/notrack/master/malicious-sites.txt
    * https://ransomwaretracker.abuse.ch/downloads/RW_DOMBL.txt
    * https://ransomwaretracker.abuse.ch/downloads/CW_C2_DOMBL.txt
    * https://ransomwaretracker.abuse.ch/downloads/LY_C2_DOMBL.txt
    * https://ransomwaretracker.abuse.ch/downloads/TC_C2_DOMBL.txt
    * https://ransomwaretracker.abuse.ch/downloads/TL_C2_DOMBL.txt
    * https://zeustracker.abuse.ch/blocklist.php?download=domainblocklist
    * https://v.firebog.net/hosts/Shalla-mal.txt
    * https://raw.githubusercontent.com/StevenBlack/hosts/master/data/add.Risk/hosts
    * https://www.squidblacklist.org/downloads/dg-malicious.acl
    * https://v.firebog.net/hosts/Airelle-hrsk.txt

* Other Lists

    * https://github.com/chadmayfield/pihole-blocklists/raw/master/lists/pi_blocklist_porn_all.list
    * https://raw.githubusercontent.com/chadmayfield/pihole-blocklists/master/lists/pi_blocklist_porn_top1m.list
    * https://zerodot1.gitlab.io/CoinBlockerLists/hosts
